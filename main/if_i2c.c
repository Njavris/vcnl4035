#include "if_i2c.h"

#include "driver/i2c.h"
#include "sdkconfig.h"

static void if_i2c_write(struct i2c_dev *dev,  uint8_t addr, uint16_t data) {
	debug("%s: %02x=%04x\n", __func__, addr, data);
	i2c_cmd_handle_t cmd = i2c_cmd_link_create();
	i2c_master_start(cmd);
	i2c_master_write_byte(cmd, dev->dev_id << 1 | I2C_MASTER_WRITE, 1);
	i2c_master_write_byte(cmd, addr, 1);
	i2c_master_write_byte(cmd, (uint8_t)(data & 0xff), 1);
	i2c_master_write_byte(cmd, (uint8_t)((data >> 8) & 0xff), 1);
	i2c_master_stop(cmd);
	i2c_master_cmd_begin(dev->port, cmd, 1000 / portTICK_RATE_MS);
	i2c_cmd_link_delete(cmd);
}

static uint16_t if_i2c_read(struct i2c_dev *dev, uint8_t addr) {
	uint8_t ret[2];
	i2c_cmd_handle_t cmd = i2c_cmd_link_create();
	i2c_master_start(cmd);
	i2c_master_write_byte(cmd, dev->dev_id << 1 | I2C_MASTER_WRITE, 1);
	i2c_master_write_byte(cmd, addr, 1);
	i2c_master_cmd_begin(dev->port, cmd, 1000 / portTICK_RATE_MS);
	i2c_cmd_link_delete(cmd);

	cmd = i2c_cmd_link_create();
	i2c_master_start(cmd);
	i2c_master_write_byte(cmd, dev->dev_id << 1 | I2C_MASTER_READ, 1);
	i2c_master_read_byte(cmd, &ret[0], 0);
	i2c_master_read_byte(cmd, &ret[1], 0);
	i2c_master_stop(cmd);
	i2c_master_cmd_begin(dev->port, cmd, 1000 / portTICK_RATE_MS);
	i2c_cmd_link_delete(cmd);
	debug("%s: %02x=%04x\n", __func__, addr, *((uint16_t*)ret));
	return *((uint16_t*)ret);
}

int if_i2c_init(unsigned port, uint8_t dev_id, unsigned sda_pin, unsigned sck_pin,
		unsigned sck_hz, struct i2c_dev *dev) {
	i2c_config_t conf = {
		.mode = I2C_MODE_MASTER,
		.sda_io_num = sda_pin,
		.sda_pullup_en = GPIO_PULLUP_ENABLE,
		.scl_io_num = sck_pin,
		.scl_pullup_en = GPIO_PULLUP_ENABLE,
		.master.clk_speed = sck_hz
	};

	dev->port = port;
	dev->dev_id = dev_id;
	dev->write = if_i2c_write;
	dev->read = if_i2c_read;

	i2c_param_config(port, &conf);
	return (i2c_driver_install(port, conf.mode, 0, 0, 0) != ESP_OK);
}

