#include "vcnl4035.h"

#ifdef DEBUG
#include "stdio.h"
#endif
#include "string.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "if_i2c.h"

static TaskHandle_t hvcnl4035 = NULL;
static struct i2c_dev i2c;

static struct vcnl4035_dev {
	int baseline_ps[3];
	int measurments[CONFIG_MAX_MEASURMENTS];
	int measurment_cnt;
	int als;
	int white;
	int last_stat_gesture;
	int last_pat_gesture;
} sensor;

char *gesture_names[] = {
	[GESTURE_NONE] = "none",
	[GESTURE_LEFT] = "left",
	[GESTURE_RIGHT] = "right",
	[GESTURE_UP] = "up",
	[GESTURE_DOWN] = "down",
};

int vcnl4035_get_gesture_stat(char **name) {
	int ret = sensor.last_stat_gesture;
	sensor.last_stat_gesture = GESTURE_NONE;
	if (name) {
		*name = gesture_names[ret];
	}
	return ret;
}

int vcnl4035_get_gesture_pat(char **name) {
	int ret = sensor.last_pat_gesture;
	sensor.last_pat_gesture = GESTURE_NONE;
	if (name) {
		*name = gesture_names[ret];
	}
	return ret;
}

enum vcnl4035_regs {
	ALS_CONF = 0,	// 0x0
	ALS_THDH,	// 0x1
	ALS_THDL,	// 0x2
	PS_CONF,	// 0x3
	PS_CONF_MS,	// 0x4
	PS_CANC,	// 0x5
	PS_THDL,	// 0x6
	PS_THDH,	// 0x7
	PS1_DATA,	// 0x8
	PS2_DATA,	// 0x9
	PS3_DATA,	// 0xa
	ALS_DATA,	// 0xb
	WHITE_DATA,	// 0xc
	INT_FLAG,	// 0xd
	ID,		// 0xe
};

static void i2c_regset(uint8_t addr, uint16_t set, uint16_t clear) {
	uint16_t reg_val, new_val;
	reg_val = i2c.read(&i2c, addr);
	new_val = reg_val;
	new_val &= ~clear;
	new_val |= set;
	if (reg_val != new_val)
		i2c.write(&i2c, addr, new_val);
}

#ifdef DEBUG
static void dump_regs(void) {
	for(int i = 0; i < 0xf; i++)
		debug("%02x:%04x ", i, i2c.read(&i2c, i));
	debug("\n");

//	debug("bl_ps: %04x %04x %04x\n", sensor.baseline_ps[0],
//					sensor.baseline_ps[1],
//					sensor.baseline_ps[2]);
}
#else
#define dump_regs()
#endif

static void setup(void) {
	int sum[3] = {0, 0, 0};

	i2c.write(&i2c, PS_CONF, BIT(14) | BIT(15));
	i2c.write(&i2c, PS_CONF_MS, BIT(0) | BIT(3));

	i2c.write(&i2c, PS_CONF_MS, BITL(3) | BITH(0));
	i2c.write(&i2c, PS_CONF, (4 << 1) | (1 << 6) | (2 << 8) 
			| BITH(2) | BITH(3) | BITH(6));
	i2c.write(&i2c, ALS_CONF, (2 << 5));
	i2c.write(&i2c, PS_THDL, 0x1ff);

	/* estimate baseline PS value */
	for (int i = 0; i < 10; i++) {
		i2c_regset(PS_CONF_MS, BITL(2), 0);
        	vTaskDelay(100 / portTICK_PERIOD_MS);
		for (int j = 0; j < 3; j++)
			sum[i] += i2c.read(&i2c, PS1_DATA + i);
	}

	for (int i = 0; i < 3; i++)
		sensor.baseline_ps[i] = sum[i] / 10;
}

static void measurment_clear(void) {
	sensor.measurment_cnt = 0;
	memset(sensor.measurments, 0x0, 8 * sizeof(int));
}

static void measurment_log(uint8_t measurment) {
	sensor.measurments[sensor.measurment_cnt++] = measurment;
}

static void estimate_stat_gesture(void) {
	uint8_t gest_from, gest_to, gest = GESTURE_NONE;
	struct gest_cnt {
		uint8_t measurment;
		int cnt;
	} measurments[2][8];

	for (int k = 0; k < 2; k++) {
		int measurment_cnt = sensor.measurment_cnt / 2;
		int off = measurment_cnt * k;
		for ( int i = 0; i < 8; i++) {
			measurments[k][i].measurment = i;
			measurments[k][i].cnt = 0;
		}

		for (int i = off; i < (measurment_cnt + off); i++)
			measurments[k][sensor.measurments[i]].cnt++;

		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				if (measurments[k][j].cnt < measurments[k][i].cnt) {
					measurments[k][j].cnt ^= measurments[k][i].cnt;
					measurments[k][i].cnt ^= measurments[k][j].cnt;
					measurments[k][j].cnt ^= measurments[k][i].cnt;
					measurments[k][j].measurment ^= measurments[k][i].measurment;
					measurments[k][i].measurment ^= measurments[k][j].measurment;
					measurments[k][j].measurment ^= measurments[k][i].measurment;
				}
			}
		}

		if (!measurments[k][0].cnt)
			return;

		debug("%s", (!k) ? "coming from\t" : "going to\t");

		for (int i = 0; i < 3; i++) {
			if (measurments[k][i].cnt)
				debug("%02x(%d/%d) ",measurments[k][i].measurment,
						measurments[k][i].cnt, measurment_cnt);
		}
		debug("\n");
	}

	if (measurments[0][0].measurment == 0x7)
		gest_from = measurments[0][1].measurment;
	else
		gest_from = measurments[0][0].measurment;

	if (measurments[1][0].measurment == 0x7)
		gest_to = measurments[1][1].measurment;
	else
		gest_to = measurments[1][0].measurment;


	if ((gest_from == 0) || (gest_to == 0))
		goto out;

/*
DOWN	3(011) -> 6(110) / 3(011) -> 2(010)	BIT(0) | BIT(1) -> BIT(1)
UP	2(010) -> 3(011) / 2(010) -> 1(001)	BIT(1)		-> BIT(0)
LEFT	3(011) -> 4(100) / 3(011) -> 5(101)	BIT(0) | BIT(1) -> BIT(2)
RIGHT	4(100) -> 3(011) / 4(100) -> 2(010)	BIT(2)		-> BIT(1)
*/

	if ((gest_from & BIT(1)) && (gest_to & BIT(0)))
		gest = GESTURE_UP;
	else if ((gest_from & BIT(2)) && (gest_to & BIT(1)))
		gest = GESTURE_RIGHT;
	else if ((gest_from & BIT(0)) && (gest_to & BIT(1)))
		gest = GESTURE_DOWN;
	else if ((gest_from & BIT(1)) && (gest_to & BIT(2)))
		gest = GESTURE_LEFT;

out:
	sensor.last_stat_gesture = gest;
}


struct gest_pat {
	uint8_t gest;
	int len;
	int val[4];
} patterns[] = {
	{GESTURE_LEFT, 3, {3, 7, 4}},
	{GESTURE_LEFT, 3, {7, 5, 4}},
	{GESTURE_RIGHT, 3, {7, 3, 2}},
	{GESTURE_UP, 3, {7, 3, 1}},
	{GESTURE_DOWN, 3, {3, 7, 2}},
	{GESTURE_DOWN, 3, {3, 7, 6}},
	{GESTURE_DOWN, 3, {7, 6, 2}},
};


static uint8_t pattern_match(uint8_t *data) {
	for (int i = 0; i < sizeof(patterns) / sizeof(struct gest_pat); i++) {
		struct gest_pat *curr_pat = &patterns[i];
		int match = 0;
		for (int j = 0; data[j]; j++) {
			if (data[j] == curr_pat->val[match])
				match++;
		}
		if (match == curr_pat->len)
			return curr_pat->gest;
	}
	return GESTURE_NONE;
}

static void estimate_pat_gesture(void) {
	uint8_t gest = GESTURE_NONE;
	uint8_t pattern[8];

	memset(pattern, 0, 8);
	for (int i = 0, j = 0; i < sensor.measurment_cnt; i++) {
		if (!i) {
			pattern[j++] = sensor.measurments[0];
			continue;
		}
		if (sensor.measurments[i] != pattern[j - 1]) {
			pattern[j++] = sensor.measurments[i];
			continue;
		}
	}

	if (!pattern[0])
		return;

	debug("Detected pattern: ");
	for (int i = 0; i < 8; i++) {
		if (!pattern[i]) break;
		debug("%02x ", pattern[i]);
	}
	debug("\n");
	gest = pattern_match(pattern);

	sensor.last_pat_gesture = gest;
}


static void read_sensor(void) {
	for (int j = 0; j < CONFIG_MAX_MEASURMENTS; j++) {
		int curr_data[3];
		int curr_measurment = 0;

        	vTaskDelay(10 / portTICK_PERIOD_MS);

		for (int i = 0; i < 3; i++)
			curr_data[i] = i2c.read(&i2c, PS1_DATA + i) - sensor.baseline_ps[i];

		i2c_regset(PS_CONF_MS, BIT(2), 0);

		for (int i = 0; i < 3; i++)
			if (curr_data[i] > CONFIG_PS_THLD)
				curr_measurment |= BIT(i);

		if (j == 0) {
			measurment_clear();
			if (!curr_measurment)
				break;
		} else {
			if (!curr_measurment)
				continue;

			measurment_log(curr_measurment);
		}
	}

	sensor.als= i2c.read(&i2c, ALS_DATA);
	sensor.white = i2c.read(&i2c, WHITE_DATA);
	i2c.read(&i2c, INT_FLAG);
	estimate_stat_gesture();
	estimate_pat_gesture();
}

static void vcnl4035_task(void *parms) {
	setup();
	while (true) {
//		dump_regs();
		read_sensor();
        	vTaskDelay(10 / portTICK_PERIOD_MS);
	}
}

void vcnl4035_init(void) {
	if_i2c_init(0, 0x60, CONFIG_VCNL_SDA, CONFIG_VCNL_SCL, CONFIG_VCNL_SCL_HZ, &i2c);
       	vTaskDelay(1000 / portTICK_PERIOD_MS);
	xTaskCreate(vcnl4035_task, "VCNL_task", CONFIG_VCNL_TASK_STACK_SIZE,
					NULL, configMAX_PRIORITIES, &hvcnl4035);
}

void vcnl4035_deinit(void) {
	if (hvcnl4035)
		vTaskDelete(hvcnl4035);
}
