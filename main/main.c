/* Hello World Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_spi_flash.h"

#include "vcnl4035.h"

void app_main(void)
{
	vcnl4035_init();
	while(1) {
        	vTaskDelay(10 / portTICK_PERIOD_MS);
		char *gest_name;
		int gest;
		gest = vcnl4035_get_gesture_stat(&gest_name);
		if (gest) printf("Statistically estimated gesture: %s\n", gest_name);

		gest = vcnl4035_get_gesture_pat(&gest_name);
		if (gest) printf("pattern estimated gesture: %s\n", gest_name);
	}
}
