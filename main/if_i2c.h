#ifndef __IF_I2C_H__
#define __IF_I2C_H__

#include "types.h"

struct i2c_dev {
	unsigned port;
	unsigned char dev_id;
	void (*write)(struct i2c_dev*, uint8_t, uint16_t);
	uint16_t (*read)(struct i2c_dev*, uint8_t);
};

int if_i2c_init(unsigned port, uint8_t dev_id, unsigned sda_pin, unsigned sck_pin,
		unsigned sck_hz, struct i2c_dev *dev);

#endif
